# order-service

## Description
This service allows to perform CRUD operations with user orders stored in MongoDB

## Project Stack
- Spring Boot
- Spring Data MongoDB
- MongoDB
- REST
- JUnit5