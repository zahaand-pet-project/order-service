package ru.zahaand.orderservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.zahaand.orderservice.dto.request.OrderRequestDto;
import ru.zahaand.orderservice.dto.response.OrderResponseDto;
import ru.zahaand.orderservice.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public ResponseEntity<List<OrderResponseDto>> getAll() {
        List<OrderResponseDto> allOrders = orderService.getAll();
        return ResponseEntity.ok(allOrders);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDto> get(
            @PathVariable String id) {
        OrderResponseDto dto = orderService.get(id);
        return ResponseEntity.ok(dto);
    }

    @PostMapping("/create")
    public ResponseEntity<OrderResponseDto> create(
            @RequestBody OrderRequestDto requestDto) {
        OrderResponseDto dto = orderService.create(requestDto);
        return ResponseEntity.ok(dto);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<OrderResponseDto> update(
            @PathVariable String id,
            @RequestBody OrderRequestDto requestDto) {
        OrderResponseDto dto = orderService.update(id, requestDto);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(
            @PathVariable String id) {
        orderService.delete(id);
        return ResponseEntity.ok().build();
    }
}
