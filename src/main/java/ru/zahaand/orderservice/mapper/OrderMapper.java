package ru.zahaand.orderservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.zahaand.orderservice.dto.request.OrderRequestDto;
import ru.zahaand.orderservice.dto.response.OrderResponseDto;
import ru.zahaand.orderservice.entity.order.Order;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderMapper ORDER_MAPPER = Mappers.getMapper(OrderMapper.class);

    Order mapDtoToOrder(OrderRequestDto dto);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "customerUuid", source = "customerUuid")
    @Mapping(target = "createAt", source = "createAt")
    @Mapping(target = "updateAt", source = "updateAt")
    @Mapping(target = "version", source = "version")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "products", source = "products")
    OrderResponseDto mapOrderToDto(Order order);
}
