package ru.zahaand.orderservice.entity.order;

public enum Status {
    CREATED,
    DELIVERY,
    COMPLETED
}
