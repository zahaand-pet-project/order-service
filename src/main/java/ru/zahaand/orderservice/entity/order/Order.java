package ru.zahaand.orderservice.entity.order;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Document(collection = "orders")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    private String id;

    @Field("customer_uuid")
    @NotNull
    private UUID customerUuid;

    @Field("create_at")
    @CreatedDate
    private Instant createAt;

    @Field("update_at")
    @LastModifiedDate
    private Instant updateAt;

    @Version
    private int version;

    private Status status = Status.CREATED;

    @NotEmpty
    private Address address;

    @NotEmpty
    private Set<Product> products;
}
