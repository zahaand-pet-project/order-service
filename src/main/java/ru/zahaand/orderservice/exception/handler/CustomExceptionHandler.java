package ru.zahaand.orderservice.exception.handler;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.zahaand.orderservice.dto.response.ExceptionResponseDto;
import ru.zahaand.orderservice.exception.OrderNotFoundException;

@RestControllerAdvice
@Slf4j
@Data
public class CustomExceptionHandler {

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<ExceptionResponseDto> handleException(Exception exception) {
        String message = exception.getMessage();
        log.error(message);
        log.trace(message, exception);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ExceptionResponseDto(message));
    }
}