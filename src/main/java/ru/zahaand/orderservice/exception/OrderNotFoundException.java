package ru.zahaand.orderservice.exception;

public class OrderNotFoundException extends RuntimeException {

    private static final String ORDER_NOT_FOUND_MESSAGE = "Order: NOT FOUND, id = %s";

    public OrderNotFoundException(String id) {
        super(String.format(ORDER_NOT_FOUND_MESSAGE, id));
    }
}
