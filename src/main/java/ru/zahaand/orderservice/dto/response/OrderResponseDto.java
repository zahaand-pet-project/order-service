package ru.zahaand.orderservice.dto.response;

import lombok.Data;
import ru.zahaand.orderservice.entity.order.Address;
import ru.zahaand.orderservice.entity.order.Product;
import ru.zahaand.orderservice.entity.order.Status;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
public class OrderResponseDto {
    String id;
    UUID customerUuid;
    Instant createAt;
    Instant updateAt;
    int version;
    Status status;
    Address address;
    Set<Product> products;
}
