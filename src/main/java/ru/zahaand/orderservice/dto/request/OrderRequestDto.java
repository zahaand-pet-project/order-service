package ru.zahaand.orderservice.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.zahaand.orderservice.entity.order.Address;
import ru.zahaand.orderservice.entity.order.Product;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
public class OrderRequestDto {
    UUID customerUuid;
    Address address;
    Set<Product> products;
}
