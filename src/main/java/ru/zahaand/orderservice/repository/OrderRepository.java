package ru.zahaand.orderservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.zahaand.orderservice.entity.order.Order;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {

}
