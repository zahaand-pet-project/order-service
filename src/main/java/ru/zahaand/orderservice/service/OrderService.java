package ru.zahaand.orderservice.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.zahaand.orderservice.dto.request.OrderRequestDto;
import ru.zahaand.orderservice.dto.response.OrderResponseDto;
import ru.zahaand.orderservice.entity.order.Order;
import ru.zahaand.orderservice.exception.OrderNotFoundException;
import ru.zahaand.orderservice.repository.OrderRepository;

import java.util.List;
import java.util.Optional;

import static ru.zahaand.orderservice.mapper.OrderMapper.ORDER_MAPPER;

/**
 * Выполняет операции CRUD над заказами в базе даных.
 * Все методы являются транзакционными.
 *
 * @see Order
 */
@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    /**
     * Получает список всех заказов из базы данных.
     * Сопоставляет список заказов со списком объектов OrderResponseDto.
     *
     * @return список всех сопоставленных с заказами объектов OrderResponseDto.
     */
    @Transactional(readOnly = true)
    public List<OrderResponseDto> getAll() {
        List<OrderResponseDto> allResponseDto = orderRepository.findAll()
                .stream()
                .map(ORDER_MAPPER::mapOrderToDto)
                .toList();
        log.info("Get all orders: SUCCESS");
        return allResponseDto;
    }

    /**
     * Получает заказ из базы данных по идентификатору.
     * Полученный заказ сопоставляется с объектом OrderResponseDto.
     *
     * @param id – идентификатор заказа для получения из базы данных.
     * @return объект OrderResponseDto, сопоставленный с полученным из базы данных заказом.
     * @throws OrderNotFoundException если заказ с указанным идентификатором отсутсвует в базе данных.
     */
    @Transactional(readOnly = true)
    public OrderResponseDto get(String id) {
        Optional<Order> order = orderRepository.findById(id);
        if (order.isPresent()) {
            OrderResponseDto responseDto = ORDER_MAPPER.mapOrderToDto(order.get());
            log.info("Get order by id: SUCCESS, id = {}", responseDto.getId());
            return responseDto;
        } else {
            throw new OrderNotFoundException(id);
        }
    }

    /**
     * Добавляет новый заказ в базу данных.
     * После сохранения заказа возвращает OrderResponseDto с идентификатором созданного заказа.
     *
     * @param dto – объект OrderRequestDto с данными нового заказа.
     * @return объект OrderResponseDto с идентификатором созданного заказа, сопоставленный с сохраненным заказом.
     */
    public OrderResponseDto create(OrderRequestDto dto) {
        Order order = ORDER_MAPPER.mapDtoToOrder(dto);
        Order savedOrder = orderRepository.save(order);
        OrderResponseDto responseDto = ORDER_MAPPER.mapOrderToDto(savedOrder);
        log.info("Create order: SUCCESS, id = {}", responseDto.getId());
        return responseDto;
    }

    /**
     * Обновляет заказ в базе данных по идентификатору.
     *
     * @param id  – идентификатор заказа для обновления в базе данных.
     * @param requestDto – объект OrderRequestDto с данными обновленного заказа.
     * @return объект OrderResponseDto, сопоставленный с обновленным в базе данных заказом.
     * @throws OrderNotFoundException если заказ с указанным идентификатором отсутсвует в базе данных.
     */
    public OrderResponseDto update(String id, OrderRequestDto requestDto) {
        Optional<Order> optional = orderRepository.findById(id);
        if (optional.isPresent()) {
            Order order = optional.get();
            order.setAddress(requestDto.getAddress());
            order.setProducts(requestDto.getProducts());
            Order updatedOrder = orderRepository.save(order);
            OrderResponseDto responseDto = ORDER_MAPPER.mapOrderToDto(updatedOrder);
            log.info("Update order: SUCCESS, id = {}", updatedOrder.getId());
            return responseDto;
        } else {
            throw new OrderNotFoundException(id);
        }
    }

    /**
     * Удаляет заказ из базы данных по идентификатору.
     *
     * @param id – идентификатор заказа для удаления в базе данных.
     * @throws OrderNotFoundException если заказ с указанным идентификатором отсутсвует в базе данных.
     */
    public void delete(String id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            orderRepository.deleteById(id);
            log.info("Delete order: SUCCESS, id = {}", id);
        } else {
            throw new OrderNotFoundException(id);
        }
    }

    /**
     * Удаляет все заказы из базы данных.
     */
    public void clear() {
        orderRepository.deleteAll();
    }
}
