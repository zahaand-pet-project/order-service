package ru.zahaand.orderservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zahaand.orderservice.dto.request.OrderRequestDto;
import ru.zahaand.orderservice.dto.response.OrderResponseDto;
import ru.zahaand.orderservice.entity.order.Address;
import ru.zahaand.orderservice.entity.order.Product;
import ru.zahaand.orderservice.exception.OrderNotFoundException;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    private static OrderRequestDto requestDto;
    private static OrderResponseDto responseDto;

    private static final int ORDERS_COUNT = 3;
    private static final int VALID_VERSION = 1;
    private static final Address VALID_ADDRESS = new Address("Country", "City", 123456);
    private static final Set<Product> VALID_PRODUCTS = Set.of(new Product(1234567890L, 3));
    private static final Address NEW_ADDRESS = new Address("New Country", "New City", 654321);
    private static final Set<Product> NEW_PRODUCTS = Set.of(new Product(9876543210L, 10));

    @BeforeEach
    void setUp() {
        orderService.clear();
        requestDto = new OrderRequestDto(UUID.randomUUID(), VALID_ADDRESS, VALID_PRODUCTS);
        responseDto = orderService.create(requestDto);
    }

    @Test
    void getAll_shouldReturnAllOrders() {
        orderService.create(requestDto);
        orderService.create(requestDto);

        List<OrderResponseDto> responseDtoList = orderService.getAll();

        assertEquals(ORDERS_COUNT, responseDtoList.size());
    }

    @Test
    void get_shouldReturnOrderById() {
        OrderResponseDto savedDto = orderService.get(responseDto.getId());

        assertEquals(requestDto.getCustomerUuid(), savedDto.getCustomerUuid());
        assertEquals(requestDto.getAddress(), savedDto.getAddress());
        assertEquals(requestDto.getProducts(), savedDto.getProducts());
    }

    @Test
    void create_shouldCreateOrder() {
        OrderResponseDto savedDto = orderService.get(responseDto.getId());

        assertEquals(requestDto.getCustomerUuid(), savedDto.getCustomerUuid());
        assertEquals(requestDto.getAddress(), savedDto.getAddress());
        assertEquals(requestDto.getProducts(), savedDto.getProducts());
        assertEquals(VALID_VERSION, savedDto.getVersion());
        assertNotNull(savedDto.getCreateAt());
        assertNotNull(savedDto.getUpdateAt());
    }

    @Test
    void update_shouldUpdateOrder() {
        requestDto.setAddress(NEW_ADDRESS);
        requestDto.setProducts(NEW_PRODUCTS);

        OrderResponseDto updatedDto = orderService.update(responseDto.getId(), requestDto);

        assertEquals(requestDto.getAddress(), updatedDto.getAddress());
        assertEquals(requestDto.getProducts(), updatedDto.getProducts());
    }

    @Test
    void delete_shouldDeleteOrder() {
        String id = responseDto.getId();

        orderService.delete(id);

        assertThrows(OrderNotFoundException.class, () -> orderService.get(id));
    }
}